# Materials and Datasheet

Add pairs of Quotation/Bill and Material/safety datasheet for each component or
material employed.

## List of materials (alphabetic Order)

__3G USB-dongle__: [Bill](3G-Dongle_bill.pdf); [Datasheet](3G-Dongle_datasheet.pdf)

---

__ARcare 92712__: [Bill](ARclad 92712_bill.pdf); [Datasheet](ARclad 92712_datasheet.pdf)

__ARseal 90697__: [Bill](ARseal 90697_bill.pdf); [Datasheet](ARseal 90697_datasheet.pdf)

---

__FO-Keyence: FU-57TZ (2000)__: [Bill](FU-57TZ 2000.pdf); [Datasheet](MaterialX_datasheet.pdf)
