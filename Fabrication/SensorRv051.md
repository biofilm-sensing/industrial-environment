# Photolithography


# Waveguide Transfer


# Milling: Fiber-Optics (FO) grooves & through-holes | Measurement Zone


# Assembly and optical alignment | Fixation
__Materials__:
[FO](), [Ebecryl600](), [PET/Milar](), [Holder]()!

__EPIs__:
[Radiation Safety glasses (blocking 365 nm)]()

![Photo of after milling with annotations](PMMA-WGs-milling.png)

1. Insert and pass FO through hole
2. Bend along milled curve to adjust protruding length
3. Place PET (with holder?) and press FO into groove
4. Inject Ebecryl600
5. Put on safety glasses
6. Irradiate during 30 s
7. Remove PET (and holder)

Repeat for second and third FO:

![Photo after FO fixation](PMMA-WGs-FO.png)

# Encapsulation in metallic screw
__Materials__:
[PMMA 5mm](),[PMMA black, 5mm](), [PSA ARseal 90697](), [Ebecryl600](), [PET/Milar]()| [Holder2]()!, [O-Ring](), [2-component Epoxy RS]()
[Epoxy 2-components (Alberto)](), 2 [Dowel-pins (pasadores) 3mm]()

__Tools__:
[Epilog Mini 24W CO2-laser cutter](), [Thorlabs 365nm light source (pistol)](), [Reamer (escariador) 3mm](), [Cutter/FO cleaver](),
[screw thread metric plug tap set](), [Corel Draw X](), 2 [clamps]()

1. Fabricate scaffold to sustain sensor surface (PMMA with WGs) inside screw
    - Build upon Corel Draw design file [Rv051]()
    - Laser cutting conditions for 5mm thick PMMA: Speed 5% | Power 90% | Frequency 5k    
2. Fabricate "opto-mechanical interface"   
    - Laser cutting conditions for 5mm thick PMMA: Speed 5% | Power 95% | Frequency 5k
    - Laser cutting conditions for PSA [ARseal 90697](): Speed 5% | Power 5% | Frequency 5k
    - Use Corel Draw design file [Rv 051]() to cut parts (without previously removing protection liners where possible)
    - open up 2.75 mm holes in [PMMA 5mm]() part with reamer to fit dowel-pins
    - carve screw thread in 2.5 mm hole in the [PMMA 5mm]() part with manual M3 metric plug
    - conserve parts without joining them until _after_ application of epoxy resin
3.
