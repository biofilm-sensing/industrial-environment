#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 16:31:37 2019

@author: nils
"""
import numpy as np
import time
import threading
from ipywidgets import  BoundedFloatText, Button, VBox, HBox, Layout, interact
from IPython.display import display

def drill_through_hole(Stage, z, coordinates):
    start = input('Reached local origin. Proceed? (y/n)')
    if start == 'n':
        pass
    else:
        Stage.set_velocity(.25)
        while True:
            try:
                z.move_relative(1.5)
                z.move_relative(-.5)
                more = input('more? y/n')
                if more=='n':
                    break
            except KeyboardInterrupt:
                break
        Stage.set_velocity(1.)
        z.move_global(0)

# ==================================================
# Rotation of coordinate system
# ==================================================
def R(phi):
    """Matrix for counterclockwise rotation"""
    if phi>0:
        R = np.array([[np.cos(phi), -np.sin(phi)],
                      [np.sin(phi),  np.cos(phi)]])
    else:
        R = np.array([[np.cos(phi),  np.sin(phi)],
                      [-np.sin(phi), np.cos(phi)]])
    return R

def calculate_transforms(reference_point):
    """Calculate transformation matrix for stretching or squeezing
    from a Point relative to (0,0)"""
    angle = np.arctan(reference_point[1]/reference_point[0])
    dx = reference_point[0]*np.cos(angle)
    dz = reference_point[2]
    def z_inclination(x):
        return dz/dx*x
    # the theoretical value of reference point x-coordinate is 22.6 mm
    stretch_factor = dx/22.6
    return [angle, z_inclination, stretch_factor]

def transform_coordinates(points, transforms):
    assert type(points) is list, 'provide list of two points'
    rotation_angle = transforms[0]
    z_inclination = transforms[1]
    stretch_factor = transforms[2]
    transformed_points = []
    for point in points:
        rotated = np.dot(R(rotation_angle),point)
        S = np.array([[stretch_factor,0],[0,stretch_factor]])
        rotated_stretched = list(np.dot(S, rotated))
        dz = z_inclination(rotated_stretched[0])
        rotated_stretched.append(dz)
        transformed_points.append(rotated_stretched)
    return transformed_points


class MillingTask():
    """
    Class object wrapping up process constants, coordinate evaluation
    (orientation and engraving direction of grooves) and rotation of workpiece
    coordinate system relative to that of the Stage.

    Several instances of this class can be joined in succession to perform all
    the milling tasks required on the sensor surface.

    Coordinates of points should be given relative to an origin on the
    workpiece.
    All dimensions are interpreted as milimeters.
    """
    # Variables
    z_secure = 2.5
    z_approach = 1.

    positioning_buffer = 0.2
    radius = 2
    bent_groove_xy = np.linspace(0, radius)
    v_movement = 1
    v_milling = .1

    def __init__(self, Stage, points, depth, zfx, tool_diameter,
               substrate_thickness = 5.,
               through_hole=False,
               sensor=False,
               test = False):
        self.Stage = Stage
        from modules.MyStage import MyAxes
        self.x = MyAxes(Stage, x=True)
        self.y = MyAxes(Stage, y=True)
        self.z = MyAxes(Stage, z=True)
        self.xy = MyAxes(Stage, x=True, y=True)
        self.xz = MyAxes(Stage, x=True, z=True)
        self.yz = MyAxes(Stage, x=True, z=True)
        self.xyz = MyAxes(Stage, x=True, y=True, z=True)

        self.zfx = zfx
        self.safety_distance = self.positioning_buffer + tool_diameter/2
        self.substrate_thickness = substrate_thickness
        self.depth = depth
        self.sensor = sensor
        self.test = test

        Stage.set_velocity(self.v_movement)
        self.positive_movement = True
        self.evaluate(points)
        if not self.positive_movement:
            self.bent_groove_xy = -self.bent_groove_xy
            self.safety_distance = -self.safety_distance

        print('positive movement:', self.positive_movement)
        print('horizontal movement:', self.horizontal)

        input('Start Dremel. Confirm with OK to proceed...')

        if through_hole:
            self.do_through_hole()
        else:
            self.xyz.move_global(*points[0])
            self.z.move_relative(self.z_secure-self.z_approach)
            if self.horizontal:
                if self.rotated:
                    self.xy.move_relative(self.safety_distance,
                                            self.yfx(self.safety_distance))
                else:
                    self.x.move_relative(self.safety_distance)
            else:
                if self.rotated:
                    self.xy.move_relative(self.xfy(self.safety_distance),
                                        self.safety_distance)
                else:
                    self.y.move_relative(self.safety_distance)

            OK = input('Reached local Origin. Confirm with y/N to proceed...')
            if OK=='y':
                # carefully approach z-level defined by depth
                self.Stage.set_velocity(self.v_milling)
                dz = self.z_approach + self.depth
                self.z.move_relative(dz)

                if not self.test:
                    # engrave straight part of groove
                    if self.horizontal:
                        if self.rotated:
                            self.xyz.move_relative(self.length, self.yfx(self.length),
                                                           self.zfx(self.length))
                        else:
                            self.xz.move_relative(self.length, self.zfx(self.length))
                    else:
                        if self.rotated:
                            self.xyz.move_relative(self.xfy(self.length), self.length,
                                                   self.zfx(self.xfy(self.length)))
                        else:
                            self.y.move_relative(self.length)

                    if not self.sensor:
                        # proceed to bent part of FO groove
                        self.do_bent_groove_xyz()
                        self.Stage.set_velocity(self.v_movement)
                        self.z.move_global(0)

                        import json
                        with open('coords.txt', 'r') as f:
                            coords = json.load(f)
                        coords['single sensor']['through holes'].append(
                                                            self.Stage.get_pos())
                        with open('coords.txt', 'w') as f:
                            json.dump(coords, f, indent=4)

                        # bent_groove_worker = threading.Thread(
                        #                     target=self.do_bent_groove_xyz)
                        # bent_groove_worker.start()
                        # once finished that, do through-hole
                        # through_hole_worker= threading.Thread(
                        #                     target=self.do_through_hole,
                        #                 kwargs={'after':bent_groove_worker})
                        # through_hole_worker.start()

                    else:
                        self.Stage.set_velocity(self.v_movement)
                        self.z.move_global(0)
                else:
                    self.Stage.set_velocity(self.v_movement)
                    self.z.move_global(0)

            else:
                pass


    def evaluate(self, points):
        """
        Coordinate pairs passed to __init__ are evaluated to determine
        orientation and direction of the milling task.

        ¡BE CAREFUL! Order of the points provided determines the direction of
        the in-plane milling movements!
        """
        assert type(points) is list, 'Provide list of coordinate-tuples,\
        e.g. [(1,1),(1,2)]'
        for point in points:
            assert type(point) is tuple or list, 'Provide points as coordinate-tuples,\
            e.g. (1,1) or (1,1,1)'
        point0 = points[0]
        point1 = points[1]
        dx = point1[0]-point0[0]
        dy = point1[1]-point0[1]
        if dx==0 or dy==0:
            self.rotated = False
            if dx == 0 and dy!=0:
                self.horizontal = False
                self.length = dy
                if dy < 0:
                    self.positive_movement = False
            elif dx != 0 and dy==0:
                self.horizontal = True
                self.length = dx
                if dx < 0:
                    self.positive_movement = False
        else:
            self.rotated = True
            if abs(dx) < abs(dy):
                self.horizontal = False
                self.length = dy
                if dy < 0:
                    self.positive_movement = False
            elif abs(dx) > abs(dy):
                self.horizontal = True
                self.length = dx
                if dx < 0:
                    self.positive_movement = False

        def x(y):
            return dx/dy*y
        self.xfy = x

        def y(x):
            return dy/dx*x
        self.yfx = y

    def bent_groove_dz(self, xy):
        """ get z-value on circle with radius==self.radius for value of x or y
        """
        return self.radius-np.sqrt(self.radius**2-xy**2)

    def do_bent_groove_xyz(self):
        for i, val in enumerate(self.bent_groove_xy):
            if i==0:
                dx, dy, dz = 0, 0, 0
            else:
                if self.horizontal:
                    dx = val - self.bent_groove_xy[i-1]
                    if self.rotated:
                        dy = self.yfx(val) - self.yfx(self.bent_groove_xy[i-1])
                else:
                    if self.rotated:
                        dx = self.xfy(val) - self.xfy(self.bent_groove_xy[i-1])
                    dy = val - self.bent_groove_xy[i-1]
                dz = self.bent_groove_dz(val) - self.bent_groove_dz(
                                                    self.bent_groove_xy[i-1])
                print('incrementing by:')
                print(dx, dy, dz)
            self.xyz.move_relative(dx, dy, dz)


class MovementButtons():
    def __init__(self, Stage):
        self.Stage = Stage
        from modules.MyStage import MyAxes
        self.x = MyAxes(Stage, x=True)
        self.y = MyAxes(Stage, y=True)
        self.xy = MyAxes(Stage, x=True, y=True)
        self.z = MyAxes(Stage, z=True)
        # self.xz = MyAxes(Stage, x=True, z=True)
        # self.yz = MyAxes(Stage, x=True, z=True)
        # self.xyz = MyAxes(Stage, x=True, y=True, z=True)
        self.ZSteps = self.StepInput('Z-Step:')
        self.XYSteps=VBox([self.StepInput('X-Step:'),
                           self.StepInput('Y-Step:')])
        ZButtons = VBox([
                self.MButton(self.z,*[(-1, self.ZSteps)], icon='chevron-up'),
                self.MButton(self.z,*[(1, self.ZSteps)], icon='chevron-down')
                ])

        ZWidgets = HBox([self.ZSteps, ZButtons], layout = Layout(align_items='center'))

        XYButtons = VBox([HBox([self.MButton(self.xy, *[(-1,self.XYSteps.children[0]),
                                                  (1,self.XYSteps.children[1])], px=40),
                        self.MButton(self.y, *[(1, self.XYSteps.children[1])], icon='chevron-up'),
                        self.MButton(self.xy,*[(1, self.XYSteps.children[0]),
                                         (1, self.XYSteps.children[1])],px=40)],
                        layout = Layout(align_items='flex-end')),
                  HBox([self.MButton(self.x, [(-1, self.XYSteps.children[0])], icon='chevron-left'),
                        Button(disabled=True,
                              button_style='', # 'success', 'info', 'warning', 'danger' or ''
                              layout=Layout(width='60px', height='60px'),
                              icon='arrows'
                            ),
                        self.MButton(self.x, *[(1, self.XYSteps.children[0])], icon='chevron-right')]),
                  HBox([self.MButton(self.xy,*[(-1,self.XYSteps.children[0]),
                                         (-1,self.XYSteps.children[1])], px=40),
                        self.MButton(self.y, *[(-1,self.XYSteps.children[1])], icon='chevron-down'),
                        self.MButton(self.xy,*[(1, self.XYSteps.children[0]),
                                         (-1,self.XYSteps.children[1])], px=40)])],
                        layout = Layout(align_items='center'))

        XYWidgets = HBox([self.XYSteps, XYButtons], layout = Layout(align_items='center'))
        StageWidgets = VBox([self.StageSettings(self.Stage.get_pos, icon='map-marker'),
                            self.StageSettings(self.Stage.set_home, icon='home')])
        display(HBox([StageWidgets,ZWidgets, XYWidgets],
                layout = Layout(align_items='center')))


    def MButton(self, movements, *steps, icon='', px=60):
        def move(b):
            xy = [step[0]*step[1].value for step in steps]
            movements.move_relative(*xy)
        b = Button(
                    icon=icon,
                    disabled=False,
                    button_style='', # 'success', 'info', 'warning', 'danger' or ''
                    layout=Layout(width='%ipx'%px, height='%ipx'%px)
                    )
        b.on_click(move)
        return b

    def StepInput(self, description):
        In = BoundedFloatText(
                        value=1.,
                        min=0,
                        max=10.0,
                        step=0.01,
                        description=description,
                        layout = Layout(width='180px'),
                        disabled=False
                    )
        return In

    def StageSettings(self, func, icon='', px=60):
        def move(b):
            result = func()
            print(result)
        b = Button(
                    icon=icon,
                    disabled=False,
                    button_style='', # 'success', 'info', 'warning', 'danger' or ''
                    layout=Layout(width='%ipx'%px, height='%ipx'%px)
                    )
        b.on_click(move)
        return b
