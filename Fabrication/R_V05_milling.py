#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 16:38:19 2018

@author: nils
"""
import serial as sl
import os
import time
import json
import numpy as np
import threading

from modules.MyStage import MyStage, MyAxes
from modules.MyGraphicsWindow import MyGraphicsWindow


#=================================
def go_somewhere_safe():
    S.write(b'%f_setvel_' %(3))
    S.write(b'0_-25_-2.5_m_')
##=================================


def evaluate(points):

    assert type(points) is list, 'Provide list of coordinate-tuples,\
    e.g. [(1,1),(1,2)]'
    for point in points:
        assert type(point) is tuple or list, 'Provide points as coordinate-tuples,\
        e.g. (1,1) or (1,1,1)'
    point0 = points[0]
    point1 = points[1]
    dx = point1[0]-point0[0]
    dy = point1[1]-point0[1]
    if dx==0 or dy==0:
        rotated = False
        if dx == 0 and dy!=0:
            horizontal = False
            if dy > 0:
        elif dx != 0 and dy==0:
            horizontal = True
    else:
        rotated = True
        if dx < dy:
            horizontal = False
        elif dx > dy:
            horizontal = True

    def y(x):
        return dy/dx*x
    def x(y):
        return dx/dy*y
    return x, y, horizontal, rotated
##=================================

def z_f(x, radius):
    # define circle with radius==1
    return radius-np.sqrt(radius**2-x**2)
##=================================



def approach_z(z_i):
    while True:
        try:
            z.move_relative(z_i)
            more = input('more? y/n')
            if more=='n':
                break
        except KeyboardInterrupt:
            break
##=================================

def move_up(z_i):
    z.move_relative(-z_i)

#==============================================================================
# Variables
z_secure = 2.5
z_approach = 1.

xs = 0.2
diameter = .5
depth = .1
safety_dist = xs+diameter/2
radius = 2



points = {'single sensor': {'FO-grooves':[[(0.0, 0.0), (-2.0, 0.0)],
                                          [(21.5, 0.0),(23.5, 0.0)],
                                          [(11.0, -6.0),(11.0, -8.0)]],
                          'sensing area':[[(17.35, -0.5),(17.35, 0.0)],
                                          [(17.35,  0.5),(17.35, 0.0)]],
                         'through holes':[]
                            }
         }

import json
with open('coords.txt', 'w') as f:
    json.dump(points, f, indent=4)

with open('coords.txt', 'r') as f:
    points = json.load( f)

points['single sensor']['through holes'].append([0,0,1])
p
x, y, horizontal, rotated = evaluate(p)
x(0)
rotated
horizontal

def engrave_point(coords, depth):
    #S.go_home()

    S.set_velocity(velocity)
    input('Start Dremel. Confirm with OK to proceed...')

    xyz.move_global(coords[0], coords[1], z_secure-z_approach)

    OK = input('Reached local Origin. Confirm with y/N to proceed...')
    if OK=='y':
        S.set_velocity(.1)

        dz = z_approach + depth
        z.move_relative(dz)
        time.sleep(5)
    else:
        pass

    S.set_velocity(velocity)
    move_up(z_secure)
    #S.go_home()

    print('Finished.\n Turn OFF Dremel.')

def engrave_line(coords, depth):
    S.go_home()

    S.set_velocity(velocity)
    input('Start Dremel. Confirm with OK to proceed...')

    xyz.move_relative(coords[0][0],
                            coords[0][1],
                            z_secure-z_approach)

    S.set_velocity(depth)
    z.move_relative(z_approach + depth)
    xy.move_relative(coords[1][0]-coords[0][0],
                     coords[1][1]-coords[0][1])
    S.set_velocity(velocity)
    move_up(z_secure)
    S.go_home()
    print('Finished.\n Turn OFF Dremel.')


def engrave_horizontally(points, length, depth, tool,
                   rotated=True,
                   sensor=False,
                   left = False,
                   test = False):
    # can be used also for sensor engraving
    x_curve = np.linspace(0,radius)
    safety_dist = xs + tool/2
    if left:
        length = -length
        x_curve = -x_curve
        safety_dist = -safety_dist

    S.set_velocity(velocity)
    input('Start Dremel. Confirm with OK to proceed...')

    if rotated:
        xyz.move_global(points[0][0],points[0][1], z_secure-z_approach)
        x_fy , y_fx = evaluate(points)
        xy.move_relative(safety_dist, y_fx(safety_dist)-.05)


        OK = input('Reached local Origin. Confirm with y/N to proceed...')
        if OK=='y':
            S.set_velocity(.1)

            dz = z_approach + depth
            z.move_relative(dz)

            if not test:
                xy.move_relative(length, y_fx(length))

                if not sensor:
                    W = MyGraphicsWindow('Curve - LiveCoords')
                    Grid = W.Graph.plot(symbol='s',
                                        symbolPen = 'k',
                                        symbolBrush = 'w',
                                        symbolSize=1,
                                        #connect='pairs',
                                        #pxMode=False
                                        )
                    Grid.setData(x_curve, z_f(x_curve, radius))
                    Mill = W.Graph.plot(symbol='s',
                            symbolPen = 'k',
                            symbolBrush = 'g',
                            symbolSize=.5,
                            #connect='pairs',
                            pxMode=False
                            )
                    pos=[0,0,0]
                    def update():
                        Mill.setData([pos[0]],[pos[2]])
                    W.update = update
                    W.start()
                    def do_curve():
                        for i, val in enumerate(x_curve):
                            if i==0:
                                dx, dy, dz = 0, 0, 0
                            else:
                                dx = val - x_curve[i-1]
                                dy = y_fx(val) - y_fx(x_curve[i-1])
                                dz = z_f(val,radius) - z_f(x_curve[i-1],radius)
                                print('incrementing by:')
                                print(dx, dy, dz)

                            xyz.move_relative(dx, dy, dz)
                            pos[0] = pos[0] + dx
                            pos[1] = pos[1] + dy
                            pos[2] = pos[2] + dz
                    T = threading.Thread(target=do_curve)
                    T.start()

                    def wait():
                        while True:
                            if T.is_alive():
                                #print('working')
                                time.sleep(.2)
                            else:
                                S.set_velocity(.5)
                                z0 = S.get_pos()[-1]
                                z_final = 8
                                zs = np.linspace(z0, z_final, int(z_final-z0))
                                for i,dz in enumerate(zs):
                                    if i==0:
                                        pass
                                    else:
                                        z.move_relative(dz-zs[i-1])
                                        move_up(.5)
                                S.set_velocity(velocity)
                                z.move_global(0)
                                print('finished')
                                break

                    Wait=threading.Thread(target=wait)
                    Wait.start()
                else:
                    S.set_velocity(velocity)
                    z.move_global(0)
            else:
                S.set_velocity(velocity)
                z.move_global(0)
        else:
            pass
    else:
        pass

def engrave_vertically(points, length, depth, tool,
                   rotated=True,
                   sensor=False,
                   down = False,
                   test = False):
    # can be used also for sensor engraving
    x_curve = np.linspace(0,radius)
    safety_dist = xs + tool/2
    if down:
        length = -length
        x_curve = -x_curve
        safety_dist = -safety_dist

    S.set_velocity(velocity)
    input('Start Dremel. Confirm with OK to proceed...')

    if rotated:
        xyz.move_global(points[0][0],points[0][1], z_secure-z_approach)
        x_fy , y_fx = evaluate(points)
        xy.move_relative(x_fy(safety_dist)-.6, safety_dist-0.35)


        OK = input('Reached local Origin. Confirm with y/N to proceed...')
        if OK=='y':
            S.set_velocity(.1)

            dz = z_approach + depth
            z.move_relative(dz)

            if not test:
                xy.move_relative(x_fy(length),length)

                if not sensor:
                    W = MyGraphicsWindow('Curve - LiveCoords')
                    Grid = W.Graph.plot(symbol='s',
                                        symbolPen = 'k',
                                        symbolBrush = 'w',
                                        symbolSize=1,
                                        #connect='pairs',
                                        #pxMode=False
                                        )
                    Grid.setData(x_curve, z_f(x_curve, radius))
                    Mill = W.Graph.plot(symbol='s',
                            symbolPen = 'k',
                            symbolBrush = 'g',
                            symbolSize=.5,
                            #connect='pairs',
                            pxMode=False
                            )
                    pos=[0,0,0]
                    def update():
                        Mill.setData([pos[0]],[pos[2]])
                    W.update = update
                    W.start()
                    def do_curve():
                        for i, val in enumerate(x_curve):
                            if i==0:
                                dx, dy, dz = 0, 0, 0
                            else:
                                dx = x_fy(val) - x_fy(x_curve[i-1])
                                dy = val - x_curve[i-1]
                                dz = z_f(val,radius) - z_f(x_curve[i-1],radius)
                                print('incrementing by:')
                                print(dx, dy, dz)

                            xyz.move_relative(dx, dy, dz)
                            pos[0] = pos[0] + dx
                            pos[1] = pos[1] + dy
                            pos[2] = pos[2] + dz
                    T = threading.Thread(target=do_curve)
                    T.start()

                    def wait():
                        while True:
                            if T.is_alive():
                                #print('working')
                                time.sleep(.2)
                            else:
                                S.set_velocity(.5)
                                z0 = S.get_pos()[-1]
                                z_final = 8
                                zs = np.linspace(z0, z_final, int(z_final-z0))
                                for i,dz in enumerate(zs):
                                    if i==0:
                                        pass
                                    else:
                                        z.move_relative(dz-zs[i-1])
                                        move_up(.5)
                                S.set_velocity(velocity)
                                z.move_global(0)
                                print('finished')
                                break

                    Wait=threading.Thread(target=wait)
                    Wait.start()
                else:
                    S.set_velocity(velocity)
                    z.move_global(0)
            else:
                S.set_velocity(velocity)
                z.move_global(0)
        else:
            pass
    else:
        pass


if __name__=='__main__':

    S.go_home()

# =============================================================================
#   Engrave alignment marks
# =============================================================================

    #for mark in marks['horizontal']['marks_left']:
    #    print(mark)
    #    engrave_mark(mark, 0.1)
    #for mark in marks['horizontal']['marks_right']:
    #    print(mark)
    #    engrave_mark(mark, 0.1)

# =============================================================================
#
# =============================================================================
