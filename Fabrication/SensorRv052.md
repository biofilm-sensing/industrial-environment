
# Photolithography

----
# Waveguide Transfer

---
# Milling: Fiber-Optics (FO) grooves & through-holes | Measurement Zone
__Materials__: PMMA Substrate with embedded polymeric Waveguides (WGs)

__Tools__: 0.6 mm end-mill, 1.0 mm ball end-mill/drill, Dremel mounted on Micos MyStage

Milling is performed according to the following scheme on single sensor basis:

![Milling scheme](Milling_single-sensor_v0.52.png)

Milling tasks ([as defined here](Milling_Rv052.py)) and [coordinates](coords.txt) are divided according to employed tools in "FO-grooves" and "sensor area", as detailed in [this ipython notebook](Automated_Milling.ipynb).

By setting the Stage's coordinate system origin as indicated in the schematic and providing the "real" position of a reference point along the horizontal axis, substrate rotation, stretching/squeezing and inclination is corrected for automatically while going through the code-cells of the [notebook](Automated_Milling.ipynb).

Ideally, we would mount a camera below the engraving table in order to observe _- in perspective -_
the positioning of the milling tool while setting reference points. 

---
# Assembly and optical alignment | Fixation
__Materials__:
[FO](), [Ebecryl600](), [PET/Milar](), [Holder]()!

__EPIs__:
[Radiation Safety glasses (blocking 365 nm)]()

![Photo of after milling with annotations](PMMA-WGs-milling.png)

1. Insert and pass FO through hole
2. Bend along milled curve to adjust protruding length
3. Place PET (with holder?) and press FO into groove
4. Inject Ebecryl600
5. Put on safety glasses
6. Irradiate during 30 s
7. Remove PET (and holder)

Repeat for second and third FO:

![Photo after FO fixation](PMMA-WGs-FO.png)

# Encapsulation in metallic screw
__Materials__:
[PMMA 5mm](),[PMMA black, 5mm](), [PSA ARseal 90697](), [Ebecryl600](), [PET/Milar]()| [Holder2]()!, [O-Ring](), [2-component Epoxy RS]()
[Epoxy 2-components (Alberto)](), 2 [Dowel-pins (pasadores) 3mm]()

__Tools__:
[Epilog Mini 24W CO2-laser cutter](), [Thorlabs 365nm light source (pistol)](), [Reamer (escariador) 3mm](), [Cutter/FO cleaver](),
[screw thread metric plug tap set](), [Corel Draw X](), 2 [clamps]()

1. Fabricate scaffold to sustain sensor surface (PMMA with WGs) inside screw
    - Build upon Corel Draw design file [Rv052/Scaffold]()
    - Laser cutting conditions for 5mm thick PMMA: Speed 5% | Power 90% | Frequency 5k    
2. Fabricate "opto-mechanical interface (OMI)"   
    - Laser cutting conditions for 5mm thick PMMA: Speed 5% | Power 95% | Frequency 5k
    - Lasercutting conditions for PSA [ARseal 90697](): Speed 5% | Power 5% | Frequency 5k
    - Use Corel Draw design file [Rv052/OMI]() to cut parts (without previously removing protection liners where possible)
    - open up 2.75 mm holes in [PMMA 5mm]() part with reamer to fit dowel-pins
    - carve screw thread in 2.5 mm hole in the [PMMA 5mm]() part with manual M3 metric plug
    - conserve parts without joining them until _after_ application of epoxy resin
3.
