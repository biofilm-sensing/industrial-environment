# CASSA

Biofilm Monitoring System  deployed in the installations of CASSA (Aigues de Sabadell)

---

## Active systems

[clientRPi@CASSA|EDAR](https://gitlab.com/python-for-device-control/database/tree/clientRPi@CASSA%7CEDAR)

- __Address__: [Planta Depuradora "EDAR"](geo:)
- __Installed Sensor__: [Rv051](/Fabrication/Sensor Rv051/)
- __Experiments__:
    - [BetaTest1.0-Rv051@CASSA|EDAR](/Installation/BetaTest 1.0/) - finished.
    - [BetaTest2.0-Rv051@CASSA|EDAR](/Installation/BetaTest 2.0/) - _ongoing_
