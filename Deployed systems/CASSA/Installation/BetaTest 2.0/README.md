# BetaTest 2.0

In contrast to BetaTest 1.0, electronic parts are now all nicely packaged to
withstand the corrosive environment (hopefully) a while longer:
* Added O-ring to opto-mechanical interface (OMI) of sensor screw:
* Applied silicone to seal the interfaces between the 3 layers of PMMA enclosing
  the LED-mounted PCB:
* Power supply unit (PSU), Spectrometer and LED-driver circuit are stowed away
safely in a poly-carbonate electrical enclosure

Also, a [3G USB-dongle](/Materials and Datasheets/README.md) was added to allow an approximation of remote
maintenance combining [Firware updates](https://gitlab.com/python-for-device-control/pydevctrl-firmware)
and [data upstream pushing on client-side](https://gitlab.com/python-for-device-control/database).
The employed [SIM-card](SIM_Movistar8420547003821.jpg) providing 3G-connectivity was purchased through CASSA.
